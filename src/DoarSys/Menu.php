<?php

namespace DoarSys;

class Menu
{
    public function getModulos()
    {
        return [
            'agenda' => 'Agenda',
            'comunica' => 'Comunica',
            'atenda' => 'Atenda',
            'solicita' => 'Solicita',
            'coleta' => 'Coleta',
            'campanha' => 'Campanha',
            'administracao' => 'Administração'
        ];
    }

    public function getAcoes()
    {
        return [
            'agenda' => [
                'agendar_doacao' => new MenuItem('agenda', 'agendar_doacao', 'Agendar Doação', true),
                'cadastrar' => new MenuItem('agenda', 'cadastrar', 'Cadastrar-se', true)
            ],
            'comunica' => [
                'comunicar_evento' => new MenuItem('comunica', 'comunicar_evento', 'Comunicar evento', false),
                'visualizar_exames' => new MenuItem('comunica', 'visualizar_exames', 'Visualizar exames por laboratório', false),
                'visualizar_servicos' => new MenuItem('comunica', 'visualizar_servicos', 'Visualizar serviços por laboratório', false),
                'visualizar_doacoes' => new MenuItem('comunica', 'visualizar_doacoes', 'Visualizar doações por laboratório', false),
            ],
            'atenda' => [
                'atender_solicitacao' => new MenuItem('atenda', 'atender_solicitacao', 'Atender solicitação de bolsa de sangue', false),
                'rastrear_bolsa' => new MenuItem('atenda', 'rastrear_bolsa', 'Rastrear solicitação de bolsa de sangue', true),
                'visualizar_andamento_solicitacao' => new MenuItem('atenda', 'visualizar_andamento_solicitacao', 'Visualizar andamento da solicitacão', false)
            ],
            'solicita' => [
                'solicitar_bolsa' => new MenuItem('solicita', 'solicitar_bolsa', 'Solicitar bolsa de sangue', false),
                'preencher_dados_recebedor' => new MenuItem('solicita', 'preencher_dados_recebedor', 'Preencher dados do recebedor', false)
            ],
            'coleta' => [
                'visualizar_resultados_exame' =>  new MenuItem('coleta', 'visualizar_resultados_exame', 'Visualizar resultados de exame', false),
                'avaliar_atendimento' =>  new MenuItem('coleta', 'avaliar_atendimento', 'Avaliar qualidade do atendimento', false),
                'enviar_resultados_exame' =>  new MenuItem('coleta', 'enviar_resultados_exame', 'Enviar resultados de exame ao SUS', false)
            ],
            'campanha' => [
                'gerenciar' => new MenuItem('campanha', 'gerenciar', 'Gerenciar campanhas de doação', false)
            ],
            'administracao' => [
                'conveniar_unidade_hospitalar' => new MenuItem('administracao', 'conveniar_unidade_hospitalar', 'Conveniar unidade hospitalar', false),
                'conveniar_laboratorio' => new MenuItem('administracao', 'conveniar_laboratorio', 'Conveniar laboratório de análise clínica', false)
            ]
        ];
    }
}