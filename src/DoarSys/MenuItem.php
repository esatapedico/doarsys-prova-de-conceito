<?php

namespace DoarSys;

class MenuItem
{
    const ROTA_PADRAO = 'modulo.pagina';

    /**
     * @var string
     */
    private $modulo;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $nome;

    /**
     * @var bool
     */
    private $ativo;

    public function __construct(string $modulo, string $slug, string $nome, bool $ativo)
    {
        $this->modulo = $modulo;
        $this->slug = $slug;
        $this->nome = $nome;
        $this->ativo = $ativo;
    }

    public function getRoute(): string
    {
        return $this->ativo ? "{$this->modulo}.{$this->slug}" : self::ROTA_PADRAO;
    }

    public function getParams(): array
    {
        return $this->ativo ? [] : ['modulo' => $this->modulo, 'pagina' => $this->slug];
    }

    /**
     * @return string
     */
    public function getNome(): string
    {
        return $this->nome;
    }
}