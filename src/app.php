<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\LocaleServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new LocaleServiceProvider());
$app->register(new TranslationServiceProvider(), [
    'translator.domains' => [],
]);

$app['menu'] = function () {
    return new \DoarSys\Menu();
};

$app['twig'] = $app->extend('twig', function ($twig, $app) {
    /** @var \Twig\Environment $twig */
    // add custom globals, filters, tags, ...
    $twig->addGlobal('menu', $app['menu']);

    return $twig;
});

return $app;
