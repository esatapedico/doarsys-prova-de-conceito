<?php

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//Request::setTrustedProxies(array('127.0.0.1'));
$app->get('/', function () use ($app) {
    return $app['twig']->render(
        'index.html.twig',
        [
            'pagina' => 'inicio',
            'modulo' => 'portal'
        ]
    );
})
->bind('homepage')
;

$app->get('/{modulo}', function (string $modulo) use ($app) {
    return $app['twig']->render(
        'index.html.twig',
        [
            'pagina' => 'inicio',
            'modulo' => $modulo
        ]
    );
})
->bind('modulo.homepage')
;

$app->get('/agenda/agendar_doacao', function (Request $request) use ($app) {

    /** @var \Symfony\Component\Form\FormFactory $formFactory */
    $formFactory = $app['form.factory'];

    $form = $formFactory->createBuilder(FormType::class, [])
        ->add('local', ChoiceType::class, [
            'choices' => ['Lab. Dra. Fulana de Tal' => 1, 'Lab. CoisaTech' => 2, 'Lab. Sanguessuga' => 3],
            'label' => 'Local: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 500%'],
            'attr' => ['style' => 'width: 50%; height: 25px']
        ])
        ->add('data', DateType::class, [
            'label' => 'Data: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 300%'],
            'attr' => ['style' => 'width: 50%; height: 25px']
        ])
        ->add('hora', TimeType::class, [
            'label' => 'Hora: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 300%'],
            'attr' => ['style' => 'width: 50%; height: 25px']
        ])
        ->add('observacoes', TextareaType::class, [
            'label' => 'Observações: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 300%'],
            'attr' => ['style' => 'width: 50%; height: 100px; border-color: darkgray']
        ])
        ->add('submit', SubmitType::class, [
            'label' => 'Enviar',
            'attr' => ['style' => 'width: 100px; height: 30px']
        ])
        ->getForm();

//    $form->handleRequest($request);
//
//    if ($form->isValid()) {
//        $data = $form->getData();
//
//        // do something with the data
//
//        // redirect somewhere
//        return $app->redirect('...');
//    }

    return $app['twig']->render(
        'agenda.agendar_doacao.html.twig',
        [
            'pagina' => 'agendar_doacao',
            'modulo' => 'agenda',
            'formulario' => $form->createView()
        ]
    );
})->bind('agenda.agendar_doacao');

$app->get('/agenda/cadastrar', function (Request $request) use ($app) {

    /** @var \Symfony\Component\Form\FormFactory $formFactory */
    $formFactory = $app['form.factory'];

    $form = $formFactory->createBuilder(FormType::class, [])
        ->add('nome', TextType::class, [
            'label' => 'Nome: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 500%'],
            'attr' => ['style' => 'width: 50%; height: 25px; border-color: darkgray']
        ])
        ->add('data_nascimento', DateType::class, [
            'label' => 'Data de nascimento: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 300%'],
            'attr' => ['style' => 'width: 50%; height: 25px; border-color: darkgray']
        ])
        ->add('rua', TextType::class, [
            'label' => 'Rua: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 500%'],
            'attr' => ['style' => 'width: 50%; height: 25px; border-color: darkgray']
        ])
        ->add('numero', TextType::class, [
            'label' => 'Número: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 500%'],
            'attr' => ['style' => 'width: 50%; height: 25px; border-color: darkgray']
        ])
        ->add('complemento', TextType::class, [
            'label' => 'Complemento: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 500%'],
            'attr' => ['style' => 'width: 50%; height: 25px; border-color: darkgray']
        ])
        ->add('bairro', TextType::class, [
            'label' => 'Bairro: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 500%'],
            'attr' => ['style' => 'width: 50%; height: 25px; border-color: darkgray']
        ])
        ->add('cidade', TextType::class, [
            'label' => 'Cidade: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 500%'],
            'attr' => ['style' => 'width: 50%; height: 25px; border-color: darkgray']
        ])
        ->add('UF', TextType::class, [
            'label' => 'UF: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 500%'],
            'attr' => ['style' => 'width: 10%; height: 25px; border-color: darkgray']
        ])
        ->add('rg', TextType::class, [
            'label' => 'RG: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 500%'],
            'attr' => ['style' => 'width: 50%; height: 25px; border-color: darkgray']
        ])
        ->add('cpf', TextType::class, [
            'label' => 'CPF: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 500%'],
            'attr' => ['style' => 'width: 50%; height: 25px; border-color: darkgray']
        ])
        ->add('submit', SubmitType::class, [
            'label' => 'Enviar',
            'attr' => ['style' => 'width: 100px; height: 30px']
        ])
        ->getForm();

    $form->handleRequest($request);

    return $app['twig']->render(
        'agenda.cadastrar.html.twig',
        [
            'pagina' => 'cadastrar',
            'modulo' => 'agenda',
            'formulario' => $form->createView()
        ]
    );
})->bind('agenda.cadastrar');

$app->match('/atenda/rastrear_bolsa', function (Request $request) use ($app) {

    /** @var \Symfony\Component\Form\FormFactory $formFactory */
    $formFactory = $app['form.factory'];

    $form = $formFactory->createBuilder(FormType::class, [])
        ->add('solicitacao', TextType::class, [
            'label' => 'Número da solicitação: ',
            'label_attr' => ['style' => 'font-weight: bold; line-height: 500%'],
            'attr' => ['style' => 'width: 50%; height: 25px; border-color: darkgray']
        ])
        ->add('submit', SubmitType::class, [
            'label' => 'Enviar',
            'attr' => ['style' => 'width: 100px; height: 30px']
        ])
        ->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
        return $app['twig']->render(
            'atenda.rastrear_bolsa.html.twig',
            [
                'pagina' => 'rastrear_bolsa',
                'modulo' => 'atenda',
                'formulario' => $form->createView(),
                'solicitacao' => $form->getData()['solicitacao']
            ]
        );
    }

    return $app['twig']->render(
        'atenda.rastrear_bolsa.html.twig',
        [
            'pagina' => 'rastrear_bolsa',
            'modulo' => 'atenda',
            'formulario' => $form->createView(),
            'solicitacao' => null
        ]
    );
})->bind('atenda.rastrear_bolsa');

$app->get('/{modulo}/{pagina}', function (string $modulo, string $pagina) use ($app) {
    return $app['twig']->render(
        'index.html.twig',
        [
            'pagina' => $pagina,
            'modulo' => $modulo
        ]
    );
})->bind('modulo.pagina');

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
